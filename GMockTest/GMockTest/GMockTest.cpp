// GMockTest.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "PersonalInformation.h"
#include "AccessInformationMock.h"
#include "stdafx.h"


using namespace std;
using ::testing::AtLeast;
using ::testing::Return;

class AccessInformationMock : public AccessInformation
{
public:
	MOCK_METHOD0(get_information, INFO*());
	MOCK_METHOD1(set_information, INFO*(INFO*));
	MOCK_METHOD1(print_information, void(INFO*));	
};
AccessInformationMock *acMock;
INFO* get_information()
{
	return acMock->get_information();
}

INFO* set_information(INFO* info)
{
	return acMock->set_information(info);
}

void print_information(INFO* info)
{
	acMock->print_information(info);
}

class PersonalInfoTest : public testing::Test {
public:
	void SetUp()
	{
		acMock = new AccessInformationMock();
	}
	void TearDown()
	{
		delete acMock;
	}
};

TEST_F(PersonalInfoTest, changePersonalInformationSuccess)
{
	INFO oldinfo = { "ABSD", 30, true };
	INFO newinfo = { "RADG", 26, true };

	EXPECT_CALL((*acMock), get_information()).Times(1).WillOnce(Return(&oldinfo));
	EXPECT_CALL((*acMock), print_information(&oldinfo)).Times(1);
	EXPECT_CALL((*acMock), set_information(&newinfo)).Times(1).WillOnce(Return(&oldinfo));
	EXPECT_EQ(true, change_information(&newinfo));
}
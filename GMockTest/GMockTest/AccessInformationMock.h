#pragma once
#include "AccessInformation.h"
class AccessInformation
{
public:
	virtual ~AccessInformation() {};
	virtual INFO* get_information() = 0;
	virtual INFO* set_information(INFO* info) = 0;
	virtual void print_information(INFO* info) = 0;
};
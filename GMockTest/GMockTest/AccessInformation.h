#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <stdbool.h>

	typedef struct information_structure
	{
		char *name;
		int age;
		bool married;
	}INFO;

	INFO *get_information(void);
	INFO *set_information(INFO *info);
	void print_information(INFO *info);

#ifdef __cplusplus
}
#endif

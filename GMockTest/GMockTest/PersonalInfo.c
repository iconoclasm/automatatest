#include <string.h>
#include "PersonalInformation.h"

bool change_information(INFO *newinfo)
{
	INFO *oldinfo = NULL;
	if (newinfo == NULL)
	{
		return false;
	}
	oldinfo = get_information();
	if (oldinfo == NULL)
	{
		return false;
	}
	print_information(oldinfo);
	if (set_information(newinfo) == NULL)
	{
		return false;
	}
	return true;
}
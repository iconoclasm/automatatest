package com.javacodegeeks.snippets.desktop;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
//import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JFrame;
import javax.swing.JTextField;

//0  Q = number of states
//1 Sigma = String constructed with all input character given
//2 Pi = String constructed with all output character given
//3 delta = set of {Qn_current, input, Qn_next}
//4 lambda = set of {Qn_current, input, output}
//5 Start = number
// Accepting = number
public class automata3 {
	public static int Q_n; // number of states
	public static String[] Sigma;
	public static String[] lambda;
	public static int Q_s; // Start
	public static String inp;
	public static String BufferString;
	public static void main(String args[]) throws IOException
	{
		inp = "";
		String[] res;
		File dir = new File(".");
		File fin = new File("KOR.txt");
		res = readFile1(fin);
		Q_n = Integer.parseInt(res[0]);
		boolean[] accept = new boolean[Q_n];
		Sigma = res[3].split(",");
		lambda = res[4].split(",");
		/*for (int check = 0; check < Sigma.length; check++)
		{
			System.out.println(Sigma[check]);
		}*/
		
		Q_s = Integer.parseInt(res[5]);
		Scanner tInput = new Scanner(System.in);	
	//	System.out.println(Pattern.matches("^[a-zA-Z0-9]$", "hi"));
/*		System.out.println("MM x: ");
		String got = tInput.nextLine();	
		System.out.println("\n"+got);
*/
		//---------------REALTIME INPUT-----------------
		JFrame frame = new JFrame("Key Listener");
    	Container contentPane = frame.getContentPane();
    	KeyListener listener = new KeyListener()
    	{

    		public void keyPressed(KeyEvent event) 
    		{
    			//System.out.println(printEventInfo("Key Pressed", event));
    			//Let's do some hardcoding here. case handling for BS, Space, alt, ctrl, shift.
    			if (printEventInfo("Key Pressed", event).equals("Backspace"))
    				inp = inp.replaceFirst(".$","");
    			else if(printEventInfo("Key Pressed", event).equals(" Space"))
    				inp = inp + " ";
    			else if(printEventInfo("Key Pressed", event).equals("Shift"))
    				inp = inp+"";
    			else //if(Pattern.matches("^[a-zA-Z0-9][a-zA-Z0-9]$", printEventInfo("Key Pressed", event)))
    				inp = inp+printEventInfo("Key Pressed", event).charAt(0);
//    			else
 //   				inp = inp+"";
    		//	System.out.println(printEventInfo("Key Pressed", event));
    			System.out.println("Input is now " + inp);
    			System.out.println(PrintMM(Sigma, lambda, Q_s, inp, BufferString));
    	    	
    		}
    		public void keyReleased(KeyEvent event)
    		{
    			//printEventInfo("Key Released", event);
    		}
    		public void keyTyped(KeyEvent event)
    		{
//    			printEventInfo("Key Typed", event);
    		} 
    		private String printEventInfo(String str, KeyEvent e)
    		{
    		//	System.out.println(str);
    			int code = e.getKeyCode();
				//  System.out.println("   Code: " + KeyEvent.getKeyText(code));
				//  System.out.println("   Char: " + e.getKeyChar());
			 
				  int mods = e.getModifiersEx();
	
				//System.out.println("    Mods: "
				//	  + KeyEvent.getModifiersExText(mods));
				// System.out.println("    Location: "
				//		  + keyboardLocation(e.getKeyLocation()));
	
			/*		 System.out.println("    Action? " + e.isActionKey());
					 System.out.println(e.getKeyChar()+KeyEvent.getKeyText(code));
			*/
					 if (KeyEvent.getKeyText(code).equals("Shift"))
					 {
						// System.out.println("It's shift");
						 return "Shift";
					 }
					 return e.getKeyChar()+KeyEvent.getKeyText(code);
			  }
    		public String PrintMM(String[] tFunct, String[] lambda, int start, String inp, String CurrentString)
    		{
    		//	System.out.println("Tfunct length: "+tFunct.length);
    			int CurrentState = start;
    			int PrevState = -1;
    			String output = "";
    			int Chosung = -1;
    			int ChosungSolo = 0;
    			int Jungsung = -1;
    			int Jongsung = 0;
    			int chara = 0;
    			String FinalString = "";
    			for (int strlen = 0; strlen < inp.length(); strlen++)
    			{
    				for (int le = 0; le < tFunct.length; le++)
    				{
    			//		System.out.println(inp.charAt(strlen)+", "+tFunct[le].charAt(3)+", "+tFunct[le].charAt(1)+", "+CurrentState);
    					int posCurState = 0; // Possible Current State. for handling more than 10 states. Hard coding, sorry!
    					if (tFunct[le].charAt(1) == 'a')
    						posCurState = 58;
    					else
    						posCurState = tFunct[le].charAt(1);
    					if (posCurState == CurrentState+48 && inp.charAt(strlen) == (tFunct[le].charAt(3)))
    					{
    						
    							
    						output = output+ lambda[le].charAt(5)+lambda[le].charAt(6)+lambda[le].charAt(7)+lambda[le].charAt(8)+" ";
    						if (Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)) == 77)
    						{
    						//	printa(Chosung,Jungsung,Jongsung);
    							chara = 0xAC00+(Chosung*21+Jungsung)*28+Jongsung;
    							FinalString += (char)chara;
    							Chosung = -1;
    							Jungsung = -1;
    							Jongsung = 0;
    							chara = 32;
    							FinalString += (char)chara;
    							CurrentState = 0;
    							break;
    						}
    						if (CurrentState == 0)
    							if (Jungsung == -1)
    							{
    								Chosung = Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6));
    								ChosungSolo = CodeConvert(Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)));
    								//System.out.println("ChosungSolo is "+ChosungSolo);
    								chara = 0x3130+ChosungSolo;
    							}
    							else
    								chara = 0xAC00+(Chosung*21+Jungsung)*28+Jongsung;
    						if (CurrentState == 1)
    							if (Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)) == 99)
    							{
    								
    								Jungsung = Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8));
    								chara = 0xAC00+(Chosung*21+Jungsung)*28+Jongsung;
    							}
    						
    						
    						if (CurrentState == 2 || CurrentState == 3 || CurrentState == 4 || CurrentState == 5)
    							if (Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)) != 99)
    							{
    								//받침이 될 수 없는 쌍자음 처리
    								if(Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)) == 4 ||
   										Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)) == 8 ||
										Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)) == 13)
    								{
    									chara = 0xAC00+(Chosung*21+Jungsung)*28+Jongsung;
    									FinalString = FinalString + (char)chara;
    									Chosung = Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6));
    									chara = 0;
    		    						CurrentState = 1;
    		    						System.out.println("Realtime: "+FinalString+(char)chara);
    		    						break;
    								}
    								else
    								{	
    								Jongsung = Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8));
    								chara = 0xAC00+(Chosung*21+Jungsung)*28+Jongsung;
    								}
    							}
    				//		Chosung = Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6));
 						
    						PrevState = CurrentState;
    						CurrentState = tFunct[le].charAt(5);
    						
    						//FOR MORE THAN 10 STATES---------------------------------------------------------
    						if (CurrentState == 'a')
    							CurrentState = 10;
    						else
    							CurrentState = CurrentState - 48;
    						
    						//=---------------------------------------------------------------------------------
    						//System.out.println("New state: "+CurrentState);
    						if (PrevState != 0 && CurrentState == 1)
    						{

	    							FinalString = FinalString + (char)chara;
	    							Chosung = Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6));
									ChosungSolo = CodeConvert(Integer.parseInt(""+lambda[le].charAt(5)+lambda[le].charAt(6)));
									//System.out.println(ChosungSolo);
									chara = 0x3130+ChosungSolo;
	    							Jungsung = -1;
	    							Jongsung = 0;
	    							//System.out.println((char)chara);
	    						
    						}
    						if ((CurrentState == 2 || CurrentState == 3 || CurrentState == 4 || CurrentState == 5) && (PrevState == 6 || PrevState == 7 || PrevState == 8 ||PrevState == 9))
    						{
    							chara = chara - Jongsung + JongsungRemainder(Jongsung); // ㄺ ㄽ ㄶ 등에서 마지막 자음만 제거
    							FinalString = FinalString+ (char)chara;
    							Chosung = JongsungGet(Jongsung);
    							Jungsung = Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8));
    							//System.out.println("Chosung and Jungsung is "+Chosung + ", "+Jungsung);
    							chara = 0xAC00+(Chosung*21+Jungsung)*28;
    							Jongsung = 0;
    							//System.out.println("From 9 to 5 chara: "+(char)chara);
    							
    						}

    						//중성 겹모음 정리-----------------------------------------------------
       						if (CurrentState == 5 && PrevState == 2) // ㅗ + ㅏ ㅐ ㅣ
    						{
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 0)
    							{
    								chara = chara +1*28;
    								Jungsung += 1;
    							}
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 1)
    							{
    								chara = chara +2*28;
    								Jungsung += 2;
    							}
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 20)
    							{
    								chara = chara +3*28;
    								Jungsung += 3;
    							}
    						}
       						if (CurrentState == 5 && PrevState == 3) // ㅜ + ㅓ ㅔ ㅣ
    						{
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 4)
    							{
    								chara = chara +1*28;
    								Jungsung += 1;
    							}
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 5)
    							{
    								chara = chara +2*28;
    								Jungsung += 2;
    							}
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 20)
    							{
    								chara = chara +3*28;
    								Jungsung += 3;
    							}
    						}
       						if (CurrentState == 5 && PrevState == 4) // ㅡㅣ
    						{
      							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 20)
    							{
    								chara = chara +1*28;
    								Jungsung += 1;
    							}
      						}
    						//종성 겹자음 정리--------------------------------------------------------
    						if (CurrentState == 9 && PrevState == 6) // 종성 ㄱ,ㅂ + ㅅ
    						{
    							if (Jongsung == 1) // ㄱ ㄳ
    							{
    								chara = chara +2;
    								Jongsung += 2;
    							}
    							if (Jongsung == 17)
    							{
    								chara = chara+1; // ㅂ ㅄ
    								Jongsung += 1;
    							}
    						}
    						if (CurrentState == 9 && PrevState == 7) // ㄴ + ㅈ,ㅎ
    						{
    							//System.out.println("hey"+lambda[le].charAt(7)+""+lambda[le].charAt(8));
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 22) // ㄴ ㄵ
    							{
    								chara = chara +1;
    								Jongsung += 1;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 27)
    							{
    								chara = chara+2; // ㄴ ㄶ
    								Jongsung += 2;
    							}
    						}
    						if (CurrentState == 9 && PrevState == 8) 
    						{ // ㄹ + ㄱ ㅁ ㅂ ㅅ ㅌ ㅍ ㅎ
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 1)
    							{
    								chara = chara +1;
    								Jongsung += 1;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 16)
    							{
    								chara = chara +2;
    								Jongsung += 2;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 17)
    							{
    								chara = chara +3;
    								Jongsung += 3;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 19)
    							{
    								chara = chara +4;
    								Jongsung += 4;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 25)
    							{
    								chara = chara +5;
    								Jongsung += 5;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 26)
    							{
    								chara = chara +6;
    								Jongsung += 6;
    							}
    							if (Integer.parseInt(""+lambda[le].charAt(7)+lambda[le].charAt(8)) == 27)
    							{
    								chara = chara +7;
    								Jongsung += 7;
    							}
    						}
    						
    						System.out.println("Realtime: "+FinalString+(char)chara);
    						//System.out.println("Up to now!! " + FinalString);
    						break;
    					}
    					if (le == tFunct.length-1)
    						return "한글로 완성되지 않습니다. Backspace 키로 이 메세지가 보이지 않을 때 까지 지워 주세요.";
    				}
    				
    			}
    			System.out.println("Final State: "+CurrentState);
    			return output;
    			
    		}
    		//자음 전용 유니코드로 변환
    		public int CodeConvert(int FromChosung)
    		{
    			int[] choset = {1,2,4,7,8,9,17,18,19,21,22,23,24,25,26,27,28,29,30};
   				return choset[FromChosung];
    		}
    		//겹자음에서 
    		public int JongsungRemainder(int DoubleJaum)
    		{
    			int[] remainder = {-1,0,0,1,0,4,4,0,0,8,8,8,8,8,8,8,0,0,17,0,0,0,0,0,0,0,0,0};
    			return remainder[DoubleJaum];
    		}
    		public int JongsungGet(int FromJongsung)
    		{
    			int[] take = {-1,0,1,9,2,12,18,3,5,0,6,7,9,16,17,18,6,7,9,9,10,11,12,14,15,16,17,18};
    			return take[FromJongsung];
    		}
    		public void printa(int a, int b, int c)
    		{
    			System.out.println(a);
    			System.out.println(b);
    			System.out.println(c);
    		}
    	/*	
			  private String keyboardLocation(int keybrd) 
			  {
				  switch (keybrd) 
				  {
				  	case KeyEvent.KEY_LOCATION_RIGHT:
				  		return "Right";
				  	case KeyEvent.KEY_LOCATION_LEFT:
				  		return "Left";
				  	case KeyEvent.KEY_LOCATION_NUMPAD:
				  		return "NumPad";
				  	case KeyEvent.KEY_LOCATION_STANDARD:
				  		return "Standard";
				  	case KeyEvent.KEY_LOCATION_UNKNOWN:
				  	default:
				  		return "Unknown";
				  }
			  }
		*/
    	};
    	JTextField textField = new JTextField();
    	textField.addKeyListener(listener);
    	contentPane.add(textField, BorderLayout.NORTH);
    	frame.pack();
	  	frame.setVisible(true);
		
	}
	

	private static String[] readFile1(File fin) throws IOException {
		FileInputStream fis = new FileInputStream(fin);
	 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String[] ret = new String[6];
		int i = 0;
		String line = null;
		while ((line = br.readLine()) != null) {
			//System.out.println(line);
			ret[i] = line;
			i++;
		}
		
		br.close();
		return ret;
	}
}

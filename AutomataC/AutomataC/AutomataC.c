// AutomataC.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//
#include<dos.h>
#include "stdafx.h"
#include "Automata.h"
#define MAX_LEN 1024 // 1KB for char[MAX_LEN]
#define BASE 0xAC00 // Initial unicode for hangul '가'
//State a,b,c,d are handling chosung-only input such as ㄳ ㄵ ㄶ ㄺ ㄻ ㄼ ㄽ ㄾ ㄿ ㅀ ㅄ ㅵ 
#define SIGMA "{0:r:1},{0:s:1},{0:e:1},{0:f:1},{0:a:1},{0:q:1},{0:t:1},{0:d:1},{0:w:1},{0:c:1},{0:z:1},{0:x:1},{0:v:1},{0:g:1},{0:R:1},{0:E:1},{0:Q:1},{0:T:1},{0:W:1},{1:r:1},{1:s:1},{1:e:1},{1:f:1},{1:a:1},{1:q:1},{1:t:1},{1:d:1},{1:w:1},{1:c:1},{1:z:1},{1:x:1},{1:v:1},{1:g:1},{1:R:1},{1:E:1},{1:Q:1},{1:T:1},{1:W:1},{1:h:2},{1:n:3},{1:m:4},{1:k:5},{1:i:5},{1:j:5},{1:u:5},{1:y:5},{1:b:5},{1:l:5},{1:o:5},{1:O:5},{1:p:5},{1:P:5},{2:k:5},{2:o:5},{2:l:5},{3:j:5},{3:p:5},{3:l:5},{4:l:5},{2:r:6},{2:q:6},{2:s:7},{2:f:8},{2:e:9},{2:a:9},{2:t:9},{2:d:9},{2:w:9},{2:c:9},{2:r:9},{2:x:9},{2:v:9},{2:g:9},{2:R:9},{2:T:9},{2:Q:1},{2:W:1},{2:E:1},{3:r:6},{3:q:6},{3:s:7},{3:f:8},{3:e:9},{3:a:9},{3:t:9},{3:d:9},{3:w:9},{3:c:9},{3:r:9},{3:x:9},{3:v:9},{3:g:9},{3:R:9},{3:T:9},{3:Q:1},{3:W:1},{3:E:1},{4:r:6},{4:q:6},{4:s:7},{4:f:8},{4:e:9},{4:a:9},{4:t:9},{4:d:9},{4:w:9},{4:c:9},{4:r:9},{4:x:9},{4:v:9},{4:g:9},{4:R:9},{4:T:9},{4:Q:1},{4:W:1},{4:E:1},{5:r:6},{5:q:6},{5:s:7},{5:f:8},{5:e:9},{5:a:9},{5:t:9},{5:d:9},{5:w:9},{5:c:9},{5:r:9},{5:x:9},{5:v:9},{5:g:9},{5:R:9},{5:T:9},{5:Q:1},{5:W:1},{5:E:1},{6:r:1},{6:s:1},{6:e:1},{6:f:1},{6:a:1},{6:q:1},{6:t:9},{6:d:1},{6:w:1},{6:c:1},{6:z:1},{6:x:1},{6:v:1},{6:g:1},{6:R:1},{6:T:1},{6:Q:1},{6:W:1},{6:E:1},{7:r:1},{7:s:1},{7:e:1},{7:f:1},{7:a:1},{7:q:1},{7:t:1},{7:d:1},{7:w:9},{7:c:1},{7:z:1},{7:x:1},{7:v:1},{7:g:9},{7:R:1},{7:T:1},{7:Q:1},{7:W:1},{7:E:1},{8:r:9},{8:s:1},{8:e:1},{8:f:1},{8:a:9},{8:q:9},{8:t:9},{8:d:1},{8:w:1},{8:c:1},{8:z:1},{8:x:9},{8:v:9},{8:g:9},{8:R:1},{8:T:1},{8:Q:1},{8:W:1},{8:E:1},{9:r:1},{9:s:1},{9:e:1},{9:f:1},{9:a:1},{9:q:1},{9:t:1},{9:d:1},{9:w:1},{9:c:1},{9:z:1},{9:x:1},{9:v:1},{9:g:1},{9:R:1},{9:E:1},{9:Q:1},{9:T:1},{9:W:1},{6:h:2},{6:n:3},{6:m:4},{6:k:5},{6:i:5},{6:j:5},{6:u:5},{6:y:5},{6:b:5},{6:l:5},{6:o:5},{6:O:5},{6:p:5},{6:P:5},{7:h:2},{7:n:3},{7:m:4},{7:k:5},{7:i:5},{7:j:5},{7:u:5},{7:y:5},{7:b:5},{7:l:5},{7:o:5},{7:O:5},{7:p:5},{7:P:5},{8:h:2},{8:n:3},{8:m:4},{8:k:5},{8:i:5},{8:j:5},{8:u:5},{8:y:5},{8:b:5},{8:l:5},{8:o:5},{8:O:5},{8:p:5},{8:P:5},{9:h:2},{9:n:3},{9:m:4},{9:k:5},{9:i:5},{9:j:5},{9:u:5},{9:y:5},{9:b:5},{9:l:5},{9:o:5},{9:O:5},{9:p:5},{9:P:5},{2:z:9},{3:z:9},{4:z:9},{5:z:9},{0: :0},{1: :0},{2: :0},{3: :0},{4: :0},{5: :0},{6: :0},{7: :0},{8: :0},{9: :0},{0:k:d},{0,i:d},{0:j:d},{0,u,d},{0,h,a},{0,y,d},{0,n,b},{0,b,d},{0,m,c},{0,l,d},{0,o,d},{0,O,d},{0,p,d},{0,P,d},{a:k:d},{a,i:d},{a:j:d},{a,u,d},{a,h,a},{a,y,d},{a,n,b},{a,b,d},{a,m,c},{a,l,d},{a,o,d},{a,O,d},{a,p,d},{a,P,d},{b:k:d},{b,i:d},{b:j:d},{b,u,d},{b,h,a},{b,y,d},{b,n,b},{b,b,d},{b,m,c},{b,l,d},{b,o,d},{b,O,d},{b,p,d},{b,P,d},{c:k:d},{c,i:d},{c:j:d},{c,u,d},{c,h,a},{c,y,d},{c,n,b},{c,b,d},{c,m,c},{c,l,d},{c,o,d},{c,O,d},{c,p,d},{c,P,d},{d:k:d},{d,i:d},{d:j:d},{d,u,d},{d,h,a},{d,y,d},{d,n,b},{d,b,d},{d,m,c},{d,l,d},{d,o,d},{d,O,d},{d,p,d},{d,P,d}"
#define LAMBDA "{0:r:0001},{0:s:0204},{0:e:0307},{0:f:0508},{0:a:0616},{0:q:0717},{0:t:0919},{0:d:1121},{0:w:1222},{0:c:1423},{0:z:1524},{0:x:1625},{0:v:1726},{0:g:1827},{0:R:0102},{0:E:0400},{0:Q:0800},{0:T:1020},{0:W:1388},{1:r:0001},{1:s:0204},{1:e:0307},{1:f:0508},{1:a:0616},{1:q:0717},{1:t:0919},{1:d:1121},{1:w:1222},{1:c:1423},{1:z:1524},{1:x:1625},{1:v:1726},{1:g:1827},{0:R:0102},{0:E:0400},{0:Q:0800},{0:T:1020},{0:W:1388},{1:h:9908},{1:n:9913},{1:m:9918},{1:k:9900},{1:i:9902},{1:j:9904},{1:u:9906},{1:y:9912},{1:b:9917},{1:l:9920},{1:o:9901},{1:O:9903},{1:p:9905},{1:P:9907},{2:k:9900},{2:o:9901},{2:l:9920},{3:j:9904},{3:p:9905},{3:l:9920},{4:l:9920},{2:r:0001},{2:q:0717},{2:s:0204},{2:f:0508},{2:e:0307},{2:a:0616},{2:t:0919},{2:d:1121},{2:w:1222},{2:c:1423},{2:r:0001},{2:x:1625},{2:v:1726},{2:g:1827},{2:R:0102},{2:T:1020},{2:Q:0888},{2:W:1388},{2:E:0488},{3:r:0001},{3:q:0717},{3:s:0204},{3:f:0508},{3:e:0307},{3:a:0616},{3:t:0919},{3:d:1121},{3:w:1222},{3:c:1423},{3:r:0001},{3:x:1625},{3:v:1726},{3:g:1827},{3:R:0102},{3:T:1020},{3:Q:0888},{3:W:1388},{3:E:0488},{4:r:0001},{4:q:0717},{4:s:0204},{4:f:0508},{4:e:0307},{4:a:0616},{4:t:0919},{4:d:1121},{4:w:1222},{4:c:1423},{4:r:0001},{4:x:1625},{4:v:1726},{4:g:1827},{4:R:0102},{4:T:1020},{4:Q:0888},{4:W:1388},{4:E:0488},{5:r:0001},{5:q:0717},{5:s:0204},{5:f:0508},{5:e:0307},{5:a:0616},{5:t:0919},{5:d:1121},{5:w:1222},{5:c:1423},{5:r:0001},{5:x:1625},{5:v:1726},{5:g:1827},{5:R:0102},{5:T:1020},{5:Q:0888},{5:W:1388},{5:E:0488},{6:r:0001},{6:s:0204},{6:e:0307},{6:f:0508},{6:a:0616},{6:q:0717},{6:t:0919},{6:d:1121},{6:w:1222},{6:c:1423},{6:z:1524},{6:x:1625},{6:v:1726},{6:g:1827},{6:R:0102},{6:E:0400},{6:Q:0800},{6:T:1020},{6:W:1388},{7:r:0001},{7:s:0204},{7:e:0307},{7:f:0508},{7:a:0616},{7:q:0717},{7:t:0919},{7:d:1121},{7:w:1222},{7:c:1423},{7:z:1524},{7:x:1625},{7:v:1726},{7:g:1827},{7:R:0102},{7:E:0400},{7:Q:0800},{7:T:1020},{7:W:1388},{8:r:0001},{8:s:0204},{8:e:0307},{8:f:0508},{8:a:0616},{8:q:0717},{8:t:0919},{8:d:1121},{8:w:1222},{8:c:1423},{8:z:1524},{8:x:1625},{8:v:1726},{8:g:1827},{8:R:0102},{8:E:0400},{8:Q:0800},{8:T:1020},{8:W:1388},{9:r:0001},{9:s:0204},{9:e:0307},{9:f:0508},{9:a:0616},{9:q:0717},{9:t:0919},{9:d:1121},{9:w:1222},{9:c:1423},{9:z:1524},{9:x:1625},{9:v:1726},{9:g:1827},{9:R:0102},{9:E:0400},{9:Q:0800},{9:T:1020},{9:W:1388},{6:h:9908},{6:n:9913},{6:m:9918},{6:k:9900},{6:i:9902},{6:j:9904},{6:u:9906},{6:y:9912},{6:b:9917},{6:l:9920},{6:o:9901},{6:O:9903},{6:p:9905},{6:P:9907},{7:h:9908},{7:n:9913},{7:m:9918},{7:k:9900},{7:i:9902},{7:j:9904},{7:u:9906},{7:y:9912},{7:b:9917},{7:l:9920},{7:o:9901},{7:O:9903},{7:p:9905},{7:P:9907},{8:h:9908},{8:n:9913},{8:m:9918},{8:k:9900},{8:i:9902},{8:j:9904},{8:u:9906},{8:y:9912},{8:b:9917},{8:l:9920},{8:o:9901},{8:O:9903},{8:p:9905},{8:P:9907},{9:h:9908},{9:n:9913},{9:m:9918},{9:k:9900},{9:i:9902},{9:j:9904},{9:u:9906},{9:y:9912},{9:b:9917},{9:l:9920},{9:o:9901},{9:O:9903},{9:p:9905},{9:P:9907},{2:z:1524},{3:z:1524},{4:z:1524},{5:z:1524},{0: :7777},{1: :7777},{2: :7777},{3: :7777},{4: :7777},{5: :7777},{6: :7777},{7: :7777},{8: :7777},{9: :7777},{0:k:9900},{0:i:9902},{0:j:9904},{0:u:9906},{0:h:9908},{0:y:9912},{0:n:9913},{0:b:9917},{0:m:9918},{0:l:9920},{0:o:9901},{0:O:9903},{0:p:9905},{0:P:9907},{a:k:9900},{a:i:9902},{a:j:9904},{a:u:9906},{a:h:9908},{a:y:9912},{a:n:9913},{a:b:9917},{a:m:9918},{a:l:9920},{a:o:9901},{a:O:9903},{a:p:9905},{a:P:9907},{b:k:9900},{b:i:9902},{b:j:9904},{b:u:9906},{b:h:9908},{b:y:9912},{b:n:9913},{b:b:9917},{b:m:9918},{b:l:9920},{b:o:9901},{b:O:9903},{b:p:9905},{b:P:9907},{c:k:9900},{c:i:9902},{c:j:9904},{c:u:9906},{c:h:9908},{c:y:9912},{c:n:9913},{c:b:9917},{c:m:9918},{c:l:9920},{c:o:9901},{c:O:9903},{c:p:9905},{c:P:9907},{d:k:9900},{d:i:9902},{d:j:9904},{d:u:9906},{d:h:9908},{d:y:9912},{d:n:9913},{d:b:9917},{d:m:9918},{d:l:9920},{d:o:9901},{d:O:9903},{d:p:9905},{d:P:9907}"

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <Windows.h>
//자음 전용 유니코드로 변환
/*
char** FileReader()
{
char line[6][3300];
FILE *plist = NULL;
int i = 0;
int total = 0;

fopen_s(&plist, "KOR.txt", "r");
while (fgets(line[i], 3300, plist)) {
line[i][strlen(line[i]) - 1] = '\0';
i++;
}

total = i;

for (i = 0; i < total; ++i)
printf("%s\n", line[i]);
fclose(plist);
return line;
}*/

int p2d(char a, char b)//parse to Digit
{
	return (a - 48) * 10 + (b - 48);
}

// Make a single unicode with chosung,jungsung,jongsung
int combo(int cho, int jung, int jong)
{
	return ((cho * 21 + jung) * 28 + jong);

}

//초성 코드를 초성전용 유니코드 offset으로 변환
int CodeConvert(int FromChosung)
{
	int choset[] = { 1,2,4,7,8,9,17,18,19,21,22,23,24,25,26,27,28,29,30 };
	return choset[FromChosung];
}

//겹자음에서
int JongsungRemainder(int DoubleJaum)
{
	int remainder[] = { -1,0,0,1,0,4,4,0,0,8,8,8,8,8,8,8,0,0,17,0,0,0,0,0,0,0,0,0 };
	return remainder[DoubleJaum];
}
int JongsungGet(int FromJongsung)
{
	int take[] = { -1,0,1,9,2,12,18,3,5,0,6,7,9,16,17,18,6,7,9,9,10,11,12,14,15,16,17,18 };
	return take[FromJongsung];
}

void printa(int a, int b, int c)
{
	printf("c: %d, j: %d, J: %d\n", a, b, c);
}

wchar_t* PrintMM(char* inp)
{
	char* tFunct = SIGMA;
	char* lambda = LAMBDA;
	int CurrentState = 0;
	int PrevState = -1;
	char output[MAX_LEN * 2];
	int outputIndex = 0; // also shows current length of valid output
	int Chosung = -1;
	int ChosungSolo = 0;
	int Jungsung = -1;
	int Jongsung = 0;
	int chara = 0;
	int charlen = 0; //variable for inp[strlen]
	int OddChosung = -1; // First part of odd Chosungs. e.g ㄹ for ㄺ, ㄴ for ㄵ, ㅂ for ㅄ
	int Odd = 0; // 1 or 0 to represent whether the Jaum is Odd(1) or not(0) (Odd = ㄳ, ㄵ, ㄶ, ㄻ, ㄼ, etc. 초성자리 겹자음)
	wchar_t FinalString[MAX_LEN * 2];
	for (int w = 0; w < MAX_LEN; w++)
	{
		FinalString[w] = 0;
	} // use memset!
	int FSIndex = 0;
	int me = 0; // le * 11 / 8, for lambda coordinate
	int posCurState = 0;
	int FirstTwo;
	int SecondTwo;
	//printf("I")
	for (int strlen = 0; inp[strlen] > 31; strlen++) //현재 입력한 문자열 전체를 가지고 첫 글자부터 스캔!
	{
		charlen = inp[strlen];
		//		printf("strlen: %d=============================================\n", inp[strlen]);

		// all typable symbols and numbers .!@#$%^&*()-+= 1234567890etc
		if ((charlen >= '!' && charlen <= '@') || (charlen >= '[' && charlen <= '`') || (charlen >= '{' && charlen <= '~'))
		{
			//			printf("Symbol Case\n");
			if (Chosung == -1)
			{
				chara = 0;
			}
			else if (Jungsung == -1)
			{
				if (OddChosung < 0)
					chara = 0x3130 + CodeConvert(Chosung);
				else
					OddChosung = 0;
			}
			else
			{
				if(Chosung != -2)
					chara = 0xAC00 + (Chosung * 21 + Jungsung) * 28 + Jongsung;
			}
				

			if (chara != 0)
			{

				FinalString[FSIndex] = chara;
				FSIndex++;

			}
			chara = inp[strlen];
			Chosung = -1;
			Jungsung = -1;
			Jongsung = 0;
			Odd = 0;
			CurrentState = 0;
			FinalString[FSIndex] = chara;
			FSIndex++;
			FinalString[FSIndex] = 0;
			chara = 0;

		}
		else
		{
			for (int le = 0; tFunct[le + 1] > 31; le += 8) 
			{
				posCurState = tFunct[le + 1];
				me = le * 11 / 8;

				if (posCurState == CurrentState + 48 && inp[strlen] == tFunct[le + 3])
				{
					FirstTwo = p2d(lambda[me + 5], lambda[me + 6]); // First Two digits of Four digit distinguisher '0102(ㄱ)' or '7777(space)' etc.
					SecondTwo = p2d(lambda[me + 7], lambda[me + 8]);
					//		printf("CharaF2: %c%c\n", lambda[me + 5], lambda[me + 6]);
					//			printf("locatF2: %d %d\n", (me + 5), (me + 6));
					//				printf("FirstTwo begins: %d\n", FirstTwo);
					//				printf("le me %d %d\n", le, me);
					output[outputIndex] = lambda[me + 5];
					output[outputIndex + 1] = lambda[me + 6];
					output[outputIndex + 2] = lambda[me + 7];
					output[outputIndex + 3] = lambda[me + 8];
					output[outputIndex + 4] = ' ';
					output[outputIndex + 5] = 0;
					outputIndex += 5;
					//				printf("CurState: %d\n", CurrentState);
					//				printf("posCurState: %c\n", posCurState);
					//				printf("Output now : %s\n", output);
					//				printf("FirstTwo : %d\n", FirstTwo);

					if (FirstTwo == 77) // Spacebar input
					{
						//printa(Chosung, Jungsung, Jongsung);

						if (Chosung == -1 && Jungsung == -1 && Jongsung == 0) // If multiple input of spacebar was given
						{
							chara = 0x20;
							FinalString[FSIndex] = chara;
							FSIndex++;

							CurrentState = 0;
							FinalString[FSIndex] = 0;
							break;
						}
						else if (Chosung == -2)
						{
							Chosung -= 1;
						}
						else if (Jungsung == -1)
						{
							if (OddChosung < 0)
								chara = 0x3130 + CodeConvert(Chosung);
							else
								OddChosung = 0;
						}
						else
							chara = 0xAC00 + (Chosung * 21 + Jungsung) * 28 + Jongsung;

						//					printf("chara: %x\n", chara);
						FinalString[FSIndex] = chara;
						chara = 0x20;
						FSIndex++;
						Chosung = -1;
						Jungsung = -1;
						Jongsung = 0;
						chara = 32;
						FinalString[FSIndex] = chara;
						FSIndex++;
						Odd = 0;
						CurrentState = 0;
						FinalString[FSIndex] = 0;
						break;
					}
					if (CurrentState == 0)
					{
						if (FirstTwo == 99)
						{
							Chosung = -2;
							chara = 0x314f + SecondTwo;
						}
						else if (Jungsung == -1) //첫 번째 초성을 입력하는 경우
						{
							Chosung = FirstTwo;
							ChosungSolo = CodeConvert(Chosung);
							chara = 0x3130 + ChosungSolo;
						}
						else
							chara = BASE + (Chosung * 21 + Jungsung) * 28 + Jongsung;
					}
					if (CurrentState == 1)
					{
						if (FirstTwo == 99) // 자음 다음 모음
						{
							if (Odd)
							{
								//printf("Odd\n");
								if (OddChosung == 19)
									FinalString[FSIndex] = 0x3144;//ㅄ+ㄱ,ㄷ+Moum
								else
									FinalString[FSIndex] = 0x3130 + CodeConvert(OddChosung); //그 외
								FSIndex++;
								FinalString[FSIndex] = 0;
								Odd = 0;
							}
							Jungsung = SecondTwo;
							chara = BASE + combo(Chosung, Jungsung, Jongsung);
						}
						else //자음 다음 다시 자음
						{
							if (chara == 0x3131) // ㄱ
							{
								OddChosung = 0;
								if (FirstTwo == 9) // ㄳ
								{
									chara = 0x3133;
									Odd = 1;
									Chosung = FirstTwo;
									break;
								}
								else
								{
									FinalString[FSIndex] = 0x3130 + CodeConvert(OddChosung);
									FSIndex++;
									FinalString[FSIndex] = 0;
									Chosung = FirstTwo;
									chara = 0x3130 + CodeConvert(Chosung);
									break;
								}
							}
							if (chara == 0x3134)//ㄴ
							{
								OddChosung = 2;
								if (FirstTwo == 12)//ㄵ
									chara = 0x3135;
								else if (FirstTwo == 18)//ㄶ
									chara = 0x3136;
								else
								{
									FinalString[FSIndex] = 0x3130 + CodeConvert(OddChosung);
									FSIndex++;
									FinalString[FSIndex] = 0;
									Chosung = FirstTwo;
									chara = 0x3130 + CodeConvert(Chosung);
									Odd = 0;
									break;
								}
								Chosung = FirstTwo;
								Odd = 1;
								break;
							}
							if (chara == 0x3139)//ㄹ
							{
								OddChosung = 5;
								switch (FirstTwo)
								{
								case 0:	chara = 0x313a;//ㄺ
									break;
								case 6: chara = 0x313b;//ㄻ
									break;
								case 7:	chara = 0x313c;//ㄼ
									break;
								case 9:	chara = 0x313d;//ㄽ
									break;
								case 16:chara = 0x313e;//ㄾ
									break;
								case 17:chara = 0x313f;//ㄿ
									break;
								case 18:chara = 0x3140;//ㅀ
									break;
								default:
								{
									FinalString[FSIndex] = 0x3130 + CodeConvert(OddChosung);
									FSIndex++;
									FinalString[FSIndex] = 0;
									Chosung = FirstTwo;
									chara = 0x3130 + CodeConvert(Chosung);
									Odd = 0;
									break;
								}
								}
								Chosung = FirstTwo;
								Odd = 1;
								break;
							}
							if (chara == 0x3142)//ㅂ
							{
								OddChosung = 7;
								switch (FirstTwo)
								{
								case 0:	chara = 0x3172;//ㅂㄱ
									break;
								case 3: chara = 0x3173;//ㅂㄷ
									break;
								case 9:	chara = 0x3144;//ㅄ
									break;
								case 12:chara = 0x3176;//ㅂㅈ
									break;
								case 16:chara = 0x3177;//ㅂㅌ
									break;
								default:
								{
									FinalString[FSIndex] = 0x3130 + CodeConvert(OddChosung);
									FSIndex++;
									FinalString[FSIndex] = 0;
									Chosung = FirstTwo;
									chara = 0x3130 + CodeConvert(Chosung);
									break;
								}
								}
								Chosung = FirstTwo;
								Odd = 1;
								break;
							}
							if (chara == 0x3144)//ㅄ
							{
								OddChosung = 19;
								switch (FirstTwo)
								{
								case 0:	chara = 0x3174;//ㅴ
									break;
								case 3: chara = 0x3175;//ㅵ
									break;
								default:
								{
									FinalString[FSIndex] = 0x3144;
									FSIndex++;
									FinalString[FSIndex] = 0;
									Chosung = FirstTwo;
									chara = 0x3130 + CodeConvert(Chosung);
									break;
								}
								}
								Chosung = FirstTwo;
								Odd = 1;
								break;
							}
							Odd = 0;
						}
					}

					if (CurrentState == 2 || CurrentState == 3 || CurrentState == 4 || CurrentState == 5)
					{

						if (p2d(lambda[me + 5], lambda[me + 6]) != 99)
						{
							//	printf("Ever Beeen here?????\n");
							//받침이 없는 쌍자음 ㅃ,ㅉ,ㄸ
							if (FirstTwo == 4 ||
								FirstTwo == 8 ||
								FirstTwo == 13)
							{
								chara = BASE + combo(Chosung, Jungsung, Jongsung);
								FinalString[FSIndex] = chara;
								//							printf("FinalStringContent2345: %d\n", FinalString[FSIndex]);
								FSIndex++;
								FinalString[FSIndex] = 0;
								Chosung = FirstTwo;
								chara = 0x3130 + CodeConvert(Chosung);
								CurrentState = 1;
								//							wprintf(L"RealTime2345: %s%c\n", FinalString, chara);
								break;
							}
							else
							{
								Jongsung = SecondTwo;
								chara = BASE + combo(Chosung, Jungsung, Jongsung);
							}
						}
					}
					if (CurrentState >= 49 && CurrentState <= 52) //state 'a'~'d', add offset 48 to be actual ascii
					{
						if (CurrentState == 49)// ㅗ + ㅏ ㅐ ㅣ
						{
							switch (SecondTwo)
							{
							case 0:	chara += 1; break;
							case 1:	chara += 2;	break;
							case 20: chara += 3; break;
							default: goto defau; //금기 사용
							}
						}
						else if (CurrentState == 50)// ㅜ+ㅓ ㅔ ㅣ
						{
							switch (SecondTwo)
							{
							case 4:	chara += 1; break;
							case 5:	chara += 2;	break;
							case 20: chara += 3; break;
							default: goto defau; //금기 사용
							}
						}
						else if (CurrentState == 51)// ㅡ+ㅣ
						{
							if (SecondTwo == 20)
								chara += 1;
							else
								goto defau; //금기 사용
						}
						else {
						defau:
							FinalString[FSIndex] = chara;
							FSIndex++;
							FinalString[FSIndex] = 0;
							chara = 0x314f + SecondTwo;
						}
					}

					PrevState = CurrentState;
					CurrentState = tFunct[le + 5];
					//				printf("PrevState>>>>>>>>>>>> = %d\n", PrevState);
					//				printf("<<<<<<<<<<<<New State = %c\n", CurrentState);
					//========FOR MORE THAN 10 STATES==========
					if (CurrentState == 'a')
						CurrentState = 49;
					else if (CurrentState == 'b')
						CurrentState = 50;
					else if (CurrentState == 'c')
						CurrentState = 51;
					else if (CurrentState == 'd')
						CurrentState = 52;
					else
						CurrentState -= 48;

					if (PrevState != 0 && CurrentState == 1) //이전 입력을 끝내고 새 글자로. ㅏㅏㄱ 나 각ㄱ 같은
					{
						FinalString[FSIndex] = chara;
						FSIndex++;
						FinalString[FSIndex] = 0;
						Chosung = FirstTwo;
						ChosungSolo = CodeConvert(FirstTwo);
						chara = 0x3130 + ChosungSolo;
						Jungsung = -1;
						Jongsung = 0;
					}
					if ((CurrentState == 2 || CurrentState == 3 || CurrentState == 4 || CurrentState == 5) && (PrevState == 6 || PrevState == 7 || PrevState == 8 || PrevState == 9))
					{
						chara = chara - Jongsung + JongsungRemainder(Jongsung);//ㄺ ㄽ ㄶ들에서 마지막 자음만 제거
						FinalString[FSIndex] = chara;
						FSIndex++;
						FinalString[FSIndex] = 0;
						Chosung = JongsungGet(Jongsung);
						Jungsung = SecondTwo;
						chara = BASE + (Chosung * 21 + Jungsung) * 28;
						Jongsung = 0;
					}

					if (CurrentState == 5 && PrevState == 2) // ㅗ + ㅏ ㅐ ㅣ
					{
						if (SecondTwo == 0)
						{
							chara = chara + 28;
							Jungsung += 1;
						}
						if (SecondTwo == 1)
						{
							chara = chara + 2 * 28;
							Jungsung += 2;
						}
						if (SecondTwo == 20)
						{
							chara = chara + 3 * 28;
							Jungsung += 3;
						}
					}
					if (CurrentState == 5 && PrevState == 3)
					{
						if (SecondTwo == 4)
						{
							chara = chara + 28;
							Jungsung += 1;
						}
						if (SecondTwo == 5)
						{
							chara = chara + 2 * 28;
							Jungsung += 2;
						}
						if (SecondTwo == 20)
						{
							chara = chara + 3 * 28;
							Jungsung += 3;
						}
					}
					if (CurrentState && PrevState == 4) // ㅡ ㅣ
					{
						if (SecondTwo == 20)
						{
							chara = chara + 28;
							Jungsung += 1;
						}
					}
					if (CurrentState == 9 && PrevState == 6) // 종성 ㄱ,ㅂ + ㅅ
					{
						if (Jongsung == 1) // ㄱ ㄳ
						{
							chara = chara + 2;
							Jongsung += 2;
						}
						if (Jongsung == 17)
						{
							chara = chara + 1; // ㅂ ㅄ
							Jongsung += 1;
						}
					}
					if (CurrentState == 9 && PrevState == 7) // ㄴ + ㅈ,ㅎ
					{
						//System.out.println("hey"+lambda[le].charAt(7)+""+lambda[le].charAt(8));
						if (SecondTwo == 22) // ㄴ ㄵ
						{
							chara = chara + 1;
							Jongsung += 1;
						}
						if (SecondTwo == 27)
						{
							chara = chara + 2; // ㄴ ㄶ
							Jongsung += 2;
						}
					}
					if (CurrentState == 9 && PrevState == 8)
					{ // ㄹ + ㄱ ㅁ ㅂ ㅅ ㅌ ㅍ ㅎ
						if (SecondTwo == 1)
						{
							chara = chara + 1;
							Jongsung += 1;
						}
						if (SecondTwo == 16)
						{
							chara = chara + 2;
							Jongsung += 2;
						}
						if (SecondTwo == 17)
						{
							chara = chara + 3;
							Jongsung += 3;
						}
						if (SecondTwo == 19)
						{
							chara = chara + 4;
							Jongsung += 4;
						}
						if (SecondTwo == 25)
						{
							chara = chara + 5;
							Jongsung += 5;
						}
						if (SecondTwo == 26)
						{
							chara = chara + 6;
							Jongsung += 6;
						}
						if (SecondTwo == 27)
						{
							chara = chara + 7;
							Jongsung += 7;
						}
					}
					//				printf("Current State: %d\n", CurrentState);
					//				wprintf(L"Realtime: %s%c\n", FinalString,chara);

					//				printf("%x\n", chara);

					//				printf("FinalStringContent: %d", FinalString[0]);
					break;
				}
			}
		}
	}
	/*
	for (int f = 0; FinalString[f] > 31; f++)
	{
		printf("%x, ", FinalString[f]);
	}*/

	FinalString[FSIndex] = chara;
	FinalString[FSIndex + 1] = 0;
	return FinalString;
}

#ifndef UNIT_TEST

int main()
{

	//return 0;
	_wsetlocale(LC_ALL, "");
	//wcout.imbue(locale("korean"));

	//Handle keystrokes
	char inputs[MAX_LEN];
	inputs[0] = 0;
	char ch = 0;
	int key;
	int cp = 0; //charPointer
				//	wchar_t hangul[3] = { 0xac00,0xac01,0 };
				//	wprintf(L"%s", hangul);
				//	while (1)
				//	{
				//		printf("KeyboardState: %d\n", GetKBCodePage());
				//		printf("VK_LCONTROL %d,  ", GetAsyncKeyState(VK_LCONTROL));
				//	printf("VK_HANGEUL %d  ", GetAsyncKeyState(VK_HANGEUL));
				//printf("VK_HANGUL %d\n", GetAsyncKeyState(VK_HANGUL));
				//}
	while (1)
	{
		if (cp < MAX_LEN - 1)
		{
			if (_kbhit)
			{
				if (GetAsyncKeyState(VK_LSHIFT) < 0 && GetAsyncKeyState(VK_LCONTROL) < 0)
				{
					printf("ALT!\n");
				}
				else
					ch = _getch();

				if (ch > 21 && ch < 128)
				{
					printf("%d, %d\n", GetAsyncKeyState(VK_LSHIFT), GetAsyncKeyState(VK_CONTROL));
					printf("%c\n", ch);
					inputs[cp] = ch;
					cp++;
					inputs[cp] = 0;
					printf("Current Inputs: %s, Stack: %d\n", inputs, cp);
				}
				else if (ch == 32)
				{
					printf("%c\n", ch);
					inputs[cp] = ch;
					cp++;
					inputs[cp] = 0;
					printf("Current Inputs: %s, Stack: %d\n", inputs, cp);
				}
				else if (ch == 8 && cp > 0)
				{
					inputs[cp - 1] = 0;
					cp--;
					printf("Backspace\n");
					printf("Current Inputs: %s\n", inputs);
				}
			}
		}
		else if (ch = _getch() == 8 && cp>0)
		{
			inputs[cp - 1] = 0;
			cp--;
			printf("Backspace\n");
			printf("Current Inputs: %s\n", inputs);
		}

		printf("Current Inputs Before PrintMM: %s", inputs);
		printf(".\n");
		wprintf(L"LAST PRINT: %s\n", PrintMM(inputs));
	}
	return 0;
}

#endif

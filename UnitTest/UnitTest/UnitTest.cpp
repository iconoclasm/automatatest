// UnitTest.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#define MAX_LEN 1024 // 1KB for char[MAX_LEN]
#define BASE 0xAC00 // Initial unicode for hangul '가'
#include "stdafx.h"
#include "gtest/gtest.h"
#include "../../AutomataC/AutomataC/Automata.h"

TEST(p2dtest, twentythree)
{
	EXPECT_EQ(23, p2d('2', '3'));
	EXPECT_EQ(3, p2d('0', '3'));
	EXPECT_EQ(78, p2d('7', '8'));
	EXPECT_EQ(0, p2d('0', '0'));
	EXPECT_EQ(99, p2d('9', '9'));
}
TEST(CodeConvert, chosung)
{
	EXPECT_EQ(0x3130 + CodeConvert(0), (wchar_t)L'ㄱ');
	EXPECT_EQ(0x3130 + CodeConvert(1), (wchar_t)L'ㄲ');
	EXPECT_EQ(0x3130 + CodeConvert(2), (wchar_t)L'ㄴ');
	EXPECT_EQ(0x3130 + CodeConvert(3), (wchar_t)L'ㄷ');
	EXPECT_EQ(0x3130 + CodeConvert(4), (wchar_t)L'ㄸ');
	EXPECT_EQ(0x3130 + CodeConvert(5), (wchar_t)L'ㄹ');
}
TEST(PrintMM_Basic, TEST_1_1)
{
	EXPECT_STREQ(L"가나다", PrintMM("rkskek"));
	EXPECT_STREQ(L"에스케이하이닉스", PrintMM("dptmzpdlgkdlslrtm"));
	EXPECT_STREQ(L"학점교류인턴", PrintMM("gkrwjaryfbdlsxjs"));
	EXPECT_STREQ(L"쀍뛟쒫", PrintMM("QnpfrEnpfqTnpfg"));
	EXPECT_STREQ(L"분당사무소", PrintMM("qnsekdtkanth"));
}
TEST(PrintMM_Basic, TEST_1_2) //받침이 될 수 없는 쌍자음
{
	EXPECT_STREQ(L"가ㄸ", PrintMM("rkE"));
	EXPECT_STREQ(L"까ㅉ", PrintMM("RkW"));
	EXPECT_STREQ(L"다ㅃ", PrintMM("ekQ"));
}

TEST(PrintMM_Basic_Space, TEST_2_1)
{
	EXPECT_STREQ(L"가나 다", PrintMM("rksk ek"));
	EXPECT_STREQ(L"에스케이 하이닉스", PrintMM("dptmzpdl gkdlslrtm"));
	EXPECT_STREQ(L"학점 교류 인턴", PrintMM("gkrwja ryfb dlsxjs"));
	EXPECT_STREQ(L"쀍 뛟 쒫", PrintMM("Qnpfr Enpfq Tnpfg"));
}
TEST(PrintMM_JAUM, TEST_3_1)
{
	EXPECT_STREQ(L"ㄱ ㄱ ㄱ ㄱ ㄱ", PrintMM("r r r r r"));
	EXPECT_STREQ(L"ㄱ ㄴ ㄷ ㄸ ㄹ", PrintMM("r s e E f"));
}
TEST(PrintMM_JAUM, TEST_3_2)
{
	EXPECT_STREQ(L"ㄳㄳㅂㅀ", PrintMM("rtrtqfg"));
	EXPECT_STREQ(L"ㄱㄴㄷㄻㅄㅇㅈ", PrintMM("rsefaqtdw"));
	EXPECT_STREQ(L"ㄵㄴㄷㄶㄹㄴㄺ", PrintMM("swsesgfsfr"));
	EXPECT_STREQ(L"ㄼㅅㅄㄽ", PrintMM("fqtqtft"));
}
TEST(PrintMM_JAUM, TEST_3_3)
{
	EXPECT_STREQ(L"ㄱㄱㄱㄱㄱ", PrintMM("rrrrr"));
	EXPECT_STREQ(L"ㄱㄷㅅㅁㅊ", PrintMM("retac"));
	EXPECT_STREQ(L"ㅌㅋㅁㄷㄹ", PrintMM("xzaef"));
	EXPECT_STREQ(L"ㅎㅇㄴㅅ", PrintMM("gdst"));
}
TEST(PrintMM_JAUM, TEST_3_5) //ㄳ->ㄱ사
{
	EXPECT_STREQ(L"ㄳ", PrintMM("rt"));
	EXPECT_STREQ(L"ㄱ사", PrintMM("rtk"));
	EXPECT_STREQ(L"ㄶ", PrintMM("sg"));
	EXPECT_STREQ(L"ㄴ후", PrintMM("sgn"));
	EXPECT_STREQ(L"ㄴㄳ", PrintMM("srt"));
	EXPECT_STREQ(L"ㄴㄱ소", PrintMM("srth"));
	EXPECT_STREQ(L"ㄱㄷㅄㄶ 나 ㄴ하 삯밣ㄵㄱ", PrintMM("reqtsg sk sgk tkrtqkfgswr"));
}
TEST(PrintMM_Jongsung, TEST_3_6) // ㅇㅇㅇ->ㅇㅇ아
{
	EXPECT_STREQ(L"ㅇ ㅇㅇ", PrintMM("d dd"));
	EXPECT_STREQ(L"ㅇ ㅇ아", PrintMM("d ddk"));
	EXPECT_STREQ(L"ㅇㅇㅇ", PrintMM("ddd"));
	EXPECT_STREQ(L"ㅇㅇ아", PrintMM("dddk"));
}
TEST(PrintMM_Jongsung, TEST4_1) //쌍자음받침 가능한/불가능한 경우
{
	EXPECT_STREQ(L"까", PrintMM("Rk"));
	EXPECT_STREQ(L"깎", PrintMM("RkR"));
	EXPECT_STREQ(L"까까", PrintMM("RkRk"));
	EXPECT_STREQ(L"깎ㄲ", PrintMM("RkRR"));
	EXPECT_STREQ(L"깎까", PrintMM("RkRRk"));
	EXPECT_STREQ(L"하", PrintMM("gk"));
	EXPECT_STREQ(L"핬", PrintMM("gkT"));
	EXPECT_STREQ(L"하쓰", PrintMM("gkTm"));
	EXPECT_STREQ(L"핬ㄸ", PrintMM("gkTE"));
	EXPECT_STREQ(L"핬뜨", PrintMM("gkTEm"));
	EXPECT_STREQ(L"까ㅃ", PrintMM("RkQ"));
	EXPECT_STREQ(L"까뻬", PrintMM("RkQp"));
	EXPECT_STREQ(L"까ㅉ", PrintMM("RkW"));
	EXPECT_STREQ(L"까짜", PrintMM("RkWk"));
	EXPECT_STREQ(L"테", PrintMM("xp"));
	EXPECT_STREQ(L"테ㅉ", PrintMM("xpW"));
	EXPECT_STREQ(L"테쯔", PrintMM("xpWm"));
}
TEST(PrintMM_Jongsung, TEST_4_2) // 갃 ->각사
{
	EXPECT_STREQ(L"갃", PrintMM("rkrt"));
	EXPECT_STREQ(L"각사", PrintMM("rkrtk"));
	EXPECT_STREQ(L"닳", PrintMM("ekfg"));
	EXPECT_STREQ(L"달호", PrintMM("ekfgh"));
	EXPECT_STREQ(L"낢", PrintMM("skfa"));
	EXPECT_STREQ(L"날마", PrintMM("skfak"));
	EXPECT_STREQ(L"앉", PrintMM("dksw"));
	EXPECT_STREQ(L"안즈", PrintMM("dkswm"));
	EXPECT_STREQ(L"곲", PrintMM("rhqt"));
	EXPECT_STREQ(L"곱사", PrintMM("rhqtk"));

	//겹자음이 아닌 경우
	EXPECT_STREQ(L"곡", PrintMM("rhr"));
	EXPECT_STREQ(L"곡ㄷ", PrintMM("rhre"));
	EXPECT_STREQ(L"탄", PrintMM("xks"));
	EXPECT_STREQ(L"탄ㄹ", PrintMM("xksf"));
	EXPECT_STREQ(L"밥", PrintMM("qkq"));
	EXPECT_STREQ(L"밥ㅎ", PrintMM("qkqg"));
}
TEST(PrintMM_Symbol, TEST_5_1) //기호 입력
{
	EXPECT_STREQ(L"...;%#@!#(@#)$&%!", PrintMM("...;%#@!#(@#)$&%!"));
	EXPECT_STREQ(L"132@9 302&^ %887 !46@5^", PrintMM("132@9 302&^ %887 !46@5^"));
}
TEST(PrintMM_Symbol, TEST_5_2) //기호 자음 조합
{
	EXPECT_STREQ(L"ㄱ.ㄴ!!", PrintMM("r.s!!"));
	EXPECT_STREQ(L" ㄳ; ㄴㄷ ㅎㅎ...", PrintMM(" rt; se gg..."));
}

TEST(PrintMM_Symbol, TEST_5_3) //기호 조합
{
	EXPECT_STREQ(L"\"인용 알티!\"", PrintMM("\"dlsdyd dkfxl!\""));
	EXPECT_STREQ(L"2016년 25%의 상승", PrintMM("2016sus 25%dml tkdtmd"));
}
TEST(PrintMM_Symbol, TEST_6_1) //기호 조합
{
	EXPECT_STREQ(L"ㅏㅐㅐㅑ", PrintMM("kooi"));
	EXPECT_STREQ(L"ㅚㅟㅝ", PrintMM("hlnlnj"));
}
TEST(PrintMM_MoumOnly, TEST_6_2) //모음 전용 Complex
{
	EXPECT_STREQ(L"ㅏㅑㅓㅕㅗㅛㅜㅠㅘㅙㅚㅝㅞㅟㅣㅣㅣㅑㅑㅑㅓㅓㅓㅕㅕㅕㅑㅑㅑㅔㅖㅐㅒ", PrintMM("kijuhynbhkhohlnjnpnlllliiijjjuuuiiipPoO"));
	EXPECT_STREQ(L"ㅏㅏ ㅕㅜㅏ ㅗㅓ ㅑㅐㅔ", PrintMM("kk unk hj iop"));
}
TEST(PrintMM_MoumOnly, TEST_6_3) //모음 기호 조합
{
	EXPECT_STREQ(L"324ㅚ423ㅣㅣ%#@ㅝㅣㅑ!^", PrintMM("324hl423ll%#@njli!^"));
	EXPECT_STREQ(L"324ㅚ 423ㅣㅣ%#@ㅝ ㅢㅑ!^", PrintMM("324hl 423l l%#@nj mli!^"));
}
int main(int argc, char* argv[])
{
	/*
	char line[6][3300];
	FILE *plist = NULL;
	int i = 0;
	int total = 0;

	fopen_s(&plist, "KOR.txt", "r");
	while (fgets(line[i], 3300, plist)) {
		line[i][strlen(line[i]) - 1] = '\0';
		i++;
	}

	total = i;

	for (i = 0; i < total; ++i)
		printf("%s\n", line[i]);
	fclose(plist);
	_wsetlocale(LC_ALL, L"korean");*/

	testing::InitGoogleTest(&argc, argv);
    return	RUN_ALL_TESTS();
}



